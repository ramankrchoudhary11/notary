const Joi = require("joi");

const validateCreateUser = (user) => {
  const schema = Joi.object({
    fname: Joi.string().required().trim(),
    lname: Joi.string().required().trim(),
    email: Joi.email().required().trim(),
    password: Joi.string().required().trim(),
    streetaddress: Joi.string().required().trim(),
    city: Joi.string().required().trim(),
    country: Joi.string().required().trim(),
    zip: Joi.number().required().min(6).max(6).trim(),
    phone: Joi.required().string().min(10),
  });
  return schema.validate(user);
};

module.exports = validateCreateUser;
