const validateCreateUser = require("./validateUser");
const express = require("express");
const User = require("./user");
const _ = require("lodash");
require("dotenv").config();
require("./conn");

const app = express();
app.use(express.json());
const port = process.env.PORT || 8000;

app.get("/",(req, res)=>{
  res.send("hello raman")
})

app.post("/createProfile", async (req, res) => {
  try {
    const {
      fname,
      lname,
      email,
      phone,
      password,
      cpassword,
      streetaddress,
      city,
      country,
      zip,
    } = req.body;

    if (password != cpassword)
      res.status(400).send("password and confirm password are not same");

    const valUser = {
      fname,
      lname,
      email,
      password,
      phone,
      streetaddress,
      city,
      country,
      zip,
    };
    const { error } = validateCreateUser(valUser);
    if (error) return res.status(400).send(error.details[0].message);

    const newUser = await User.findOne({ email });
    if (newUser) res.status(400).send("This user had already registered");

    const salt = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(password, salt);

    newUser = await new User({
      fname,
      lname,
      email,
      phone,
      streetaddress,
      city,
      country,
      zip,
      password: hashPassword,
    });
    await newUser.save();

    const token = newUser.generateAuthToken();

    res
      .status(201)
      .send({ data: { ..._.pick(newUser, ["_id", "name", "email"]) }, token });
  } catch (error) {
    console.log(error);
  }
});

// app.get("/me", async (req, res) => {
//   try {
//     const userData = await User.find();
//     res.status(200).send(userData);
//   } catch (err) {
//     res.status(400).send(err);
//   }
// });

app.listen(port, () => {
  console.log("web set at port no." + port);
});
